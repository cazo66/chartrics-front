FROM edermag/tomcat-8-dev

WORKDIR /intellector-ui
 
ENV app intellector-ui

COPY intellector-ui /intellector-ui

RUN mvn install -P development

RUN mv /intellector-ui/target/$app-0.0.1-SNAPSHOT.war $CATALINA_HOME/webapps/
 
CMD ["catalina.sh", "run"]