(function() {
  'use strict';

  angular
    .module('app.dashboard.message')
    .factory('messageService', messageService);

  messageService.$inject = ['$websocket', '$uibModal', 'lodash', 'toastr', '$location'];

  function messageService($websocket, $uibModal, lodash, toastr, $location) {
    var stream = $websocket('ws://' + $location.host() + ':' + $location.port() + '/chartrics/message');

    stream.onOpen(function() {
      console.log('Conexão aberta.');
    });

    stream.onClose(function() {
      console.log('Conexão fechada.');
    });

    stream.onMessage(function(message) {
      var action = JSON.parse(message.data);

      switch (action.type) {
        case 'sendMessage': {
          service.messages.push(action.data);
          toastr.success('Uma mensagem foi enviada.');

          break;
        }
        case 'deleteMessage': {
          service.messages.splice(lodash.findIndex(service.messages, function(message) {
            return lodash.isEqual(message, action.data);
          }), 1);
          toastr.error('Uma mensagem foi excluída.');

          break;
        }
      }
    });

    stream.onError(function(error) {
      console.log(error);
    });

    var service = {
      openSendMessageModal: openSendMessageModal,
      sendAction: sendAction,
      messages: []
    };

    return service;

    function openSendMessageModal() {
      $uibModal.open({
        animation: false,
        templateUrl: 'app/message/send/message-send.html',
        controller: 'MessageSendController',
        controllerAs: 'vm'
      });
    }

    function sendAction(action, data) {
      return stream.send({ type: action, data: data });
    }
  }
})();
