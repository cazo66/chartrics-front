(function() {
  'use strict';

  angular
    .module('app.dashboard.message')
    .controller('MessageListController', MessageListController);

  MessageListController.$inject = ['messageService', 'toastr'];

  function MessageListController(messageService, toastr) {
    var vm = this;

    vm.openSendMessageModal = messageService.openSendMessageModal;
    vm.deleteMessage = deleteMessage;
    vm.messages = messageService.messages;

    activate();

    function activate() {}

    function deleteMessage(message) {
      messageService.sendAction('deleteMessage', message);
    }

    // $scope.$on('$destroy', function() {
    //   messageService.closeStream();
    // });
  }
})();
