(function() {
  'use strict';

  angular
    .module('app.dashboard.message')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('app.dashboard.message', {
        url: '/mensagens',
        views: {
          'content@app.dashboard': {
            controller: 'MessageListController',
            controllerAs: 'vm',
            templateUrl: 'app/message/list/message-list.html'
          }
        }
      });
  }
})();
