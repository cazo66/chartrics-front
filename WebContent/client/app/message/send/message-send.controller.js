(function() {
  'use strict';

  angular
    .module('app.dashboard.message')
    .controller('MessageSendController', MessageSendController);

  MessageSendController.$inject = ['$uibModalInstance', 'messageService', 'toastr'];

  function MessageSendController($uibModalInstance, messageService, toastr) {
    var vm = this;

    vm.cancel = cancel;
    vm.sendMessage = sendMessage;

    activate();

    function activate() {}

    function cancel() {
    	$uibModalInstance.dismiss();
    }

    function sendMessage(message) {
      messageService.sendAction('sendMessage', message).then(function() {
        $uibModalInstance.close();
      });
    }
  }
})();
