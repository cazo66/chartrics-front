(function() {
  'use strict';

  angular
    .module('app.dashboard.chart')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('app.dashboard.chart', {
        url: '/graficos',
        views: {
          'content@app.dashboard': {
            templateUrl: 'app/chart/chart.html'
          },
          'proposal@app.dashboard.chart': {
            controller: 'ProposalController',
            controllerAs: 'vm',
            templateUrl: 'app/proposal/proposal.html'
          },
          'status@app.dashboard.chart': {
            controller: 'StatusController',
            controllerAs: 'vm',
            templateUrl: 'app/status/status.html'
          }
        }
      });
  }
})();
