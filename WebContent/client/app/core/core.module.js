(function() {
  'use strict';

  angular.module('app.core', [
    'ngWebSocket',
    'ngSanitize',
    'ngAnimate',
    'ui.router',
    'ui.router.tabs',
    'ui.utils.masks',
    'ui.bootstrap',
    'ngLodash',
    'chart.js',
    'toastr'
  ]);
})();
