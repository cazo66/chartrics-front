(function() {
  'use strict';

  angular
    .module('app.dashboard.panel')
    .controller('PanelStatusController', PanelStatusController);

  PanelStatusController.$inject = ['lodash', 'statusService', 'toastr'];

  function PanelStatusController(lodash, statusService, toastr) {
    var vm = this;

    vm.data = lodash.cloneDeep(statusService.data);
    vm.indexOf = lodash.indexOf;
    vm.labels = statusService.labels;
    vm.save = save;
    vm.monthSelected = 0;

    activate();

    function activate() {}

    function save() {
      statusService.sendAction(vm.data);
    }
  }
})();
