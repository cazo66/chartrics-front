(function() {
  'use strict';

  angular
    .module('app.dashboard.panel')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('app.dashboard.panel', {
        abstract: true,
        url: '/painel',
        views: {
          'content@app.dashboard': {
            controller: 'PanelController',
            controllerAs: 'vm',
            templateUrl: 'app/panel/panel.html'
          }
        }
      }).state('app.dashboard.panel.proposal', {
        url: '/propostas',
        controller: 'PanelProposalController',
        controllerAs: 'vm',
        templateUrl: 'app/panel/proposal/panel-proposal.html'
      }).state('app.dashboard.panel.status', {
        url: '/status',
        controller: 'PanelStatusController',
        controllerAs: 'vm',
        templateUrl: 'app/panel/status/panel-status.html'
      });
  }
})();
