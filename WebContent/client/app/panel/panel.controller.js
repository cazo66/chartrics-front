(function() {
  'use strict';

  angular
    .module('app.dashboard.panel')
    .controller('PanelController', PanelController);

  PanelController.$inject = ['$state'];

  function PanelController($state) {
    var vm = this;

    activate();

    function activate() {
      vm.tabs = [{
        heading: 'Propostas',
        route: 'app.dashboard.panel.proposal'
      }, {
        heading: 'Status das propostas',
        route: 'app.dashboard.panel.status'
      }];
    }
  }
})();
