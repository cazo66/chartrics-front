(function() {
  'use strict';

  angular
    .module('app.dashboard.panel')
    .controller('PanelProposalController', PanelProposalController);

  PanelProposalController.$inject = ['lodash', 'proposalService', 'toastr'];

  function PanelProposalController(lodash, proposalService, toastr) {
    var vm = this;

    vm.data = lodash.cloneDeep(proposalService.data);
    vm.save = save;

    activate();

    function activate() {}

    function save() {
      proposalService.sendAction(vm.data);
    }
  }
})();
