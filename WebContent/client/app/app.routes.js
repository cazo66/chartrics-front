(function() {
  'use strict';

  angular
    .module('app')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state('app', { abstract: true });
    $urlRouterProvider.otherwise('/graficos');
  }
})();
