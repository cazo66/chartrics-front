(function() {
  'use strict';

  angular
    .module('app.dashboard.chart')
    .controller('ProposalController', ProposalController);

  ProposalController.$inject = ['proposalService'];

  function ProposalController(proposalService) {
    var vm = this;

    vm.colours = proposalService.colours;
    vm.data = proposalService.data;
    vm.labels = proposalService.labels;
    vm.pending = proposalService.pending;
    vm.time = proposalService.time;
    vm.getPercentage = getPercentage;

    activate();

    function activate() {}

    function getPercentage(value) {
      return proposalService.getPercentage(vm.data, value);
    }
  }
})();
