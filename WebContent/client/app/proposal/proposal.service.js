(function() {
  'use strict';

  angular
    .module('app.dashboard.chart')
    .factory('proposalService', proposalService);

  proposalService.$inject = ['$websocket', 'lodash', 'toastr', '$location'];

  function proposalService($websocket, lodash, toastr, $location) {
    var stream = $websocket('ws://' + $location.host() + ':' + $location.port() + '/chartrics/proposal');

    stream.onOpen(function() {
      console.log('Conexão aberta.');
    });

    stream.onClose(function() {
      console.log('Conexão fechada.');
    });

    stream.onMessage(function(message) {
      toastr.info('O gráfico das propostas foi atualizado.');
      angular.copy(JSON.parse(message.data), service.data);
    });

    var service = {
      getPercentage: getPercentage,
      sendAction: sendAction,
      colours: ['#f0ad4e', '#5cb85c', '#337ab7', '#777777', '#d9534f'],
      labels: ['Propostas mesa crédito', 'Proposta aprovadas', 'Propostas controle', 'Alçada', 'Propostas negadas'],
      pending: 54,
      time: 12,
      data: []
    };

    activate();

    function activate() {
      angular.copy([40, 10, 26, 12, 19], service.data);
    }

    return service;

    function getPercentage(data, value) {
      return lodash.round(value / lodash.sum(data) * 100, 1);
    }

    function sendAction(data) {
      return stream.send(data);
    }
  }
})();
