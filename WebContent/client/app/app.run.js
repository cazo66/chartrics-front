(function() {
  'use strict';

  angular
    .module('app')
    .run(run);

  run.$inject = ['$window', '$location', '$rootScope'];

	function run($window, $location, $rootScope) {
    $window.ga('create', 'UA-78061482-1', 'auto');

    $rootScope.$on('$stateChangeSuccess', function(event) {
      $window.ga('send', 'pageview', $location.path());
    });
	}
})();
