(function() {
  'use strict';

  angular
    .module('app.dashboard.chart')
    .factory('statusService', statusService);

  statusService.$inject = ['$websocket', 'lodash', 'toastr', '$location'];

  function statusService($websocket, lodash, toastr, $location) {
    var stream = $websocket('ws://' + $location.host() + ':' + $location.port() + '/chartrics/status');

    stream.onOpen(function() {
      console.log('Conexão aberta.');
    });

    stream.onClose(function() {
      console.log('Conexão fechada.');
    });

    stream.onMessage(function(message) {
      toastr.info('O gráfico dos status das propostas foi atualizado.');
      angular.copy(JSON.parse(message.data), service.data);
    });

    var service = {
      getPercentage: getPercentage,
      sendAction: sendAction,
      getSum: getSum,
      colours: ['#5cb85c', '#d9534f'],
      data: [],
      labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      series: ['Propostas aprovadas', 'Propostas negadas']
    };

    activate();

    function activate() {
      angular.copy([
        [65, 59, 80, 81, 56, 55, 40, 71, 69, 49, 53, 62],
        [28, 48, 40, 19, 86, 27, 69, 81, 75, 46, 31, 73]
      ], service.data);
    }

    return service;

    function getPercentage(values) {
      return lodash.round(lodash.sum(values) / getSum(service.data) * 100, 1);
    }

    function getSum(values) {
      return lodash(values).flattenDeep().sum();
    }

    function sendAction(data) {
      return stream.send(data);
    }
  }
})();
