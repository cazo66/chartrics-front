(function() {
  'use strict';

  angular
    .module('app.dashboard.chart')
    .controller('StatusController', StatusController);

  StatusController.$inject = ['statusService'];

  function StatusController(statusService) {
    var vm = this;

    vm.colours = statusService.colours;
    vm.data = statusService.data;
    vm.labels = statusService.labels;
    vm.getPercentage = statusService.getPercentage;
    vm.getSum = statusService.getSum;
    vm.series = statusService.series;

    activate();

    function activate() {}
  }
})();
