(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$state', 'lodash', 'messageService'];

  function NavbarController($state, lodash, messageService) {
    var vm = this;

    vm.collapse = collapse;
    vm.isCollapsed = true;
    vm.isPanelActive = isPanelActive;
    vm.countMessages = countMessages;

    function countMessages() {
      return lodash.size(messageService.messages) + 1;
    }

    function collapse(status) {
      vm.isCollapsed = status || !vm.isCollapsed;
    }

    function isPanelActive() {
      return $state.includes('app.dashboard.panel');
    }
  }
})();
