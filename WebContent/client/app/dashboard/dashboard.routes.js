(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('app.dashboard', {
        abstract: true,
        views: {
          'navbar@app.dashboard': {
            controller: 'NavbarController',
            controllerAs: 'vm',
            templateUrl: 'app/dashboard/navbar/navbar.html'
          },
          '@': {
            templateUrl: 'app/dashboard/dashboard.html'
          }
        }
      });
  }
})();
