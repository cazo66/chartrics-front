(function() {
  'use strict';

  angular.module('app.dashboard', [
    'app.dashboard.message',
    'app.dashboard.chart',
    'app.dashboard.panel'
  ]);
})();
