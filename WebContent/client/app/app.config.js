(function() {
  'use strict';

  angular
    .module('app')
    .config(config);

  config.$inject = ['ChartJsProvider', 'toastrConfig'];

  function config(ChartJsProvider, toastrConfig) {
    ChartJsProvider.setOptions({
      colours: ['#97BBCD', '#DCDCDC', '#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      animationEasing: 'linear',
      animationSteps: 20,
    });

    angular.extend(toastrConfig, {
      timeOut: 3000
    });
  }
})();
